import 'reflect-metadata'
import { importSchema } from 'graphql-import'
import { GraphQLServer } from 'graphql-yoga'
import { resolvers } from './resolvers'
import * as path from 'path'
import { createConnection } from 'typeorm'


export async function startServer() {
    const typeDefs = importSchema(path.join(__dirname, 'schema.graphql'))
    
    const connectionName = process.env.DbCnnection || 'default'
    console.log('connection name', connectionName);
    const port = process.env.PORT || 4400
    
    let connection = await createConnection(connectionName)
    const context = { connection }
    const server = new GraphQLServer({ typeDefs, resolvers, context })

    server.start({ port }, () =>console.log(`Server is running on localhost:${port}`))
}

startServer()