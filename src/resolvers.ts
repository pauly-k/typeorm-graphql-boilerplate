import { ResolverMap } from "./types/graphql-utils";
import * as bcrypt from 'bcryptjs'
import { User } from "./entity/User";
import { Connection } from "typeorm";
 
export const resolvers: ResolverMap = {
  Query: {
    hello: (_: any, { name }: GQL.IHelloOnQueryArguments) => `Hello ${name || 'World'}`,
	},
	Mutation: {
		register: async (_: any, { email, password }: GQL.IRegisterOnMutationArguments, ctx: any ) : Promise<boolean> => {
			console.log('adding user');
			let conn = ctx.connection as Connection
			let userRepo = conn.manager.getRepository(User)
			const hashed = await bcrypt.hash(password,  10)
		  let user =	await userRepo.create({
				 email,
				 password: hashed
			})
			await userRepo.save(user);
			return  true
		} 
	}
}

