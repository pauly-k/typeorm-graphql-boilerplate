import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BaseEntity } from 'typeorm'

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({length: 255, unique: true, nullable: false}) 
  email: string

  @Column({length: 255, nullable: false}) 
  password: string

  @Column() random: number

  @BeforeInsert()
  addRandom() {
      this.random = Math.floor(Math.random() * 10000000)
  }
}
