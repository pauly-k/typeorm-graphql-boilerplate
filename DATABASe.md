# typeorm boilderplate setup

## npm packages

```bash
npm i typeorm mssql sqlite3
npm i -D typescript ts-node-dev ts-node @types/node
```

## remove docker container and volume if any

```bash
docker container stop typeorm_test
docker container rm typeorm_test
docker volume rm typeorm_test
```

## create docker container databse and appuser

docker mssql instance

```bash
docker run -e 'ACCEPT_EULA=Y' -e \
   'MSSQL_SA_PASSWORD=Lookberry8' \
   --name 'typeorm_test' -p 9306:1433 \
   -v typeorm_test:/var/opt/mssql \
   -d mcr.microsoft.com/mssql/server:2019-GA-ubuntu-16.04
```

create database and user

```bash

echo "
  create database test_typeorm
  GO
  CREATE LOGIN appuser WITH PASSWORD = 'Lookberry8'
  USE test_typeorm
  CREATE USER appuser FOR LOGIN appuser
  EXEC sp_addrolemember N'db_datareader', N'appuser'
  EXEC sp_addrolemember N'db_datawriter', N'appuser'
" > cmd.sql

sqlcmd -S localhost,9306 -U sa -P Lookberry8 -i cmd.sql
rm cmd.sql
```

## ormconfig.json

```json
[
  {
    "name": "dev",
    "type": "mssql",
    "host": "localhost",
    "port": 9306,
    "username": "sa",
    "password": "Lookberry8",
    "database": "typeorm_test",
    "synchronize": true,
    "logging": true,
    "entities": ["src/entity/**/*.ts"],
    "migrations": ["src/migration/**/*.ts"],
    "subscribers": ["src/subscriber/**/*.ts"]
  },
  {
    "name": "test",
    "type": "sqlite",
    "database": ":memory:",
    "synchronize": true,
    "logging": true,
    "entities": ["src/entity/**/*.ts"],
    "migrations": ["src/migration/**/*.ts"],
    "subscribers": ["src/subscriber/**/*.ts"]
  }
]```

## tsconfig.json

```json
{
  "compilerOptions": {
    "target": "es6",
    "module": "commonjs",
    "lib": ["dom", "es6", "es2017", "esnext.asynciterable"],
    "sourceMap": true,
    "outDir": "./dist",
    "moduleResolution": "node",
    "declaration": false,

    "composite": false,
    "removeComments": true,
    "noImplicitAny": true,
    "strictNullChecks": true,
    "strictFunctionTypes": true,
    "noImplicitThis": true,
    "noUnusedLocals": true,
    "noUnusedParameters": true,
    "noImplicitReturns": true,
    "noFallthroughCasesInSwitch": true,
    "allowSyntheticDefaultImports": false,
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "skipLibCheck": true,
    "baseUrl": ".",
    "rootDir": "src"
  },
  "exclude": ["node_modules"],
  "include": ["./src/**/*.tsx", "./src/**/*.ts"]
}
```

## list databases

```bash
sqlcmd -S localhost,9306 -U sa -P Lookberry8 -Q "select name from sys.databases"
```

## drop database and re-create

```bash
sqlcmd -S localhost,9306 -U sa -P Lookberry8 -Q "drop database [test_typeorm]"
sqlcmd -S localhost,9306 -U sa -P Lookberry8 -Q "create database [test_typeorm]"
```

## connect ineractive

```bash
sqlcmd -S localhost,9306 -U sa -P Lookberry8
```
